# AngularSuperheroes

Proyecto de prueba en Angular 17 + ngx-boostrap 12.

Se trata de una aplicación para crear, editar y eliminar superheroes de un listado.

Para realizar las operaciones CRUD se ha utilizado la siguiente API de pruebas https://crudcrud.com/.

Cada vez que accede a la página web de la API se crea automaticamenete un nuevo path independiente que nos permite cargar, editar y eliminar datos en una API vacía.
Este path de forma gratuita nos permite hacer 100 peticiones y tras 24 horas desaparecera. Una vez agotado o cáducado el path este dejara de funcionar y tendremos que conseguir otro si queremos seguir utilizando la API.

En esta aplicación el path se almacena en /src/app/services/data/data.service.ts en la variable pathEndpoins.
Para poder utilizar la aplicación tan solo tendremos que substituir el contenido de esa variable por un nuevo path.

Una vez lanzada con un path valido la aplicación nos dara la opción de cargar en la API una serie de datos predefinidos.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
