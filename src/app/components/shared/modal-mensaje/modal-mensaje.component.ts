// import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-mensaje',
  standalone: true,
  imports: [],
  templateUrl: './modal-mensaje.component.html',
  styleUrl: './modal-mensaje.component.scss',
  providers: [ BsModalService ],
})
export class ModalMensajeComponent {

  modalRef?: BsModalRef;
  error: boolean = false;
  mensaje: string = ""

  constructor() {}

  ngOnInit() {
  }

}
