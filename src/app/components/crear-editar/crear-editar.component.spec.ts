import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CrearEditarComponent } from './crear-editar.component';
import { Router, ActivatedRoute, RouterLink, RouterModule } from '@angular/router';

describe('CrearEditarComponent', () => {
  let component: CrearEditarComponent;
  let fixture: ComponentFixture<CrearEditarComponent>;

  const fakeActivatedRoute = {
    snapshot: { data: { } }
  } as ActivatedRoute;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CrearEditarComponent, HttpClientModule],
      providers: [ {provide: ActivatedRoute, useValue: fakeActivatedRoute} ]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CrearEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
