import { CommonModule, TitleCasePipe, LowerCasePipe } from '@angular/common';
import { Component, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalMensajeComponent } from '../shared/modal-mensaje/modal-mensaje.component';
import { superheroe } from '../../interfaces/superheroe';
import { superheroeAPI } from '../../interfaces/superheroeAPI';
import { DataService } from '../../services/data/data.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faFloppyDisk, faXmark } from '@fortawesome/free-solid-svg-icons';
import { FormsModule, ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-crear-editar',
  standalone: true,
  imports: [ CommonModule, FontAwesomeModule, FormsModule, ReactiveFormsModule, RouterLink ],
  templateUrl: './crear-editar.component.html',
  styleUrl: './crear-editar.component.scss',
  providers: [ BsModalService ]
})
export class CrearEditarComponent {

  faFloppyDisk = faFloppyDisk;
  faXmark = faXmark;
  modalRef?: BsModalRef;
  superheroe: superheroe = { id: "", imagen: "./../../../assets/images/sin_imagen.jpg", nombre: "", poder: "" };
  idSuperheroe: string = "";
  formularioHeroe: FormGroup
  imagenActual: string;
  crear: boolean = true;

  constructor(private dataService: DataService, private modalService: BsModalService, private router: Router) {

    this.formularioHeroe = new FormGroup({
      nombre: new FormControl('', Validators.required),
      poder: new FormControl('', Validators.required),
      imagen: new FormControl('./../../../assets/images/sin_imagen.jpg', Validators.required),
    });
    this.imagenActual = "./../../../assets/images/sin_imagen.jpg";

    if(this.router.url != "/crear") {
      this.crear = false;
      this.idSuperheroe = this.router.url.split('/')[2];
      this.dataService.get("superheroes", "/"+this.idSuperheroe).subscribe((data: any) => {
  
        this.formularioHeroe = new FormGroup({
          nombre: new FormControl(data.name, Validators.required),
          poder: new FormControl(data.age, Validators.required),
          imagen: new FormControl(data.colour, Validators.required),
        });
        this.imagenActual = data.colour;
      
      }, error => {
        this.ErrorPeticion();
      });
      
    }
  }

  crearSuperheroe() {

    if (this.formularioHeroe.valid) {
      let nuevoSuperheroe: superheroeAPI = { name: "", age: "", colour: "" };
      nuevoSuperheroe.name = this.formularioHeroe.get('nombre')?.value;
      nuevoSuperheroe.age = this.formularioHeroe.get('poder')?.value;
      nuevoSuperheroe.colour = this.formularioHeroe.get('imagen')?.value;

      this.dataService.post("superheroes", nuevoSuperheroe).subscribe((data: any[]) => {
        const initialState = {
          error: false,
          mensaje: "Superheroe "+nuevoSuperheroe.name+" creado correctamente."
        };
        this.modalRef = this.modalService.show(ModalMensajeComponent, {initialState});
        this.modalRef.content.closeBtnName = 'Close';
        this.router.navigate(['/listado']);

      }, error => {
        this.ErrorPeticion();
      });
    }
  }

  actualizarSuperheroe() {

    if (this.formularioHeroe.valid) {
      let nuevoSuperheroe: superheroeAPI = { name: "", age: "", colour: "" };
      nuevoSuperheroe.name = this.formularioHeroe.get('nombre')?.value;
      nuevoSuperheroe.age = this.formularioHeroe.get('poder')?.value;
      nuevoSuperheroe.colour = this.formularioHeroe.get('imagen')?.value;

      this.dataService.put("superheroes", "/"+this.idSuperheroe, nuevoSuperheroe).subscribe((data: any[]) => {
        const initialState = {
          error: false,
          mensaje: "Superheroe "+nuevoSuperheroe.name+" actualizado correctamente."
        };
        this.modalRef = this.modalService.show(ModalMensajeComponent, {initialState});
        this.modalRef.content.closeBtnName = 'Close';
        this.router.navigate(['/listado']);

      }, error => {
        this.ErrorPeticion();
      });
    }
  }

  imagenAleatoria() {
    this.dataService.get("imagenAleatoria").subscribe((data: any) => {
    }, response => {
      if(response.status == 200) {
        this.formularioHeroe.get('imagen')?.setValue(response.url);
      } else {
        this.ErrorPeticion();
      }
    });
  }

  ErrorPeticion() {
    const initialState = {
      error: true,
      mensaje: "A Ocurrido un error al realizar la petición. Asegurese de que dispone de peticiones para usar la API y que su path no ha caducado."
    };
    this.modalRef = this.modalService.show(ModalMensajeComponent, {initialState});
    this.modalRef.content.closeBtnName = 'Close';
  }

}
