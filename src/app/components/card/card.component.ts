import { CommonModule, TitleCasePipe, LowerCasePipe } from '@angular/common';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { superheroe } from '../../interfaces/superheroe';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [ FontAwesomeModule, CommonModule, TitleCasePipe, LowerCasePipe ],
  templateUrl: './card.component.html',
  styleUrl: './card.component.scss'
})
export class CardComponent {
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;

  @Input() superheroe: superheroe = { id: "", imagen: "", nombre: "", poder: "" };
  @Output() eventoEditar = new EventEmitter<superheroe>();
  @Output() eventoBorrar = new EventEmitter<superheroe>();

  editar() {
    this.eventoEditar.emit(this.superheroe);
  }

  borrar() {
    this.eventoBorrar.emit(this.superheroe);
  }

}
