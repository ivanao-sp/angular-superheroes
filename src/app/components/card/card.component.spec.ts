import { ComponentFixture, TestBed } from '@angular/core/testing';
import { superheroe } from '../../interfaces/superheroe';
import { CardComponent } from './card.component';
import { asNativeElements } from '@angular/core';

describe('MyCardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('debe emitir eventoEditar con el botón editar', () => {

    component.superheroe = {"id": "a2", "nombre": "Manolo", "poder": "superfuerza", "imagen": "www.example.com/image.jpg"};

    fixture.detectChanges();
    spyOn( component.eventoEditar, 'emit');

    const btnEditar = compiled.querySelector('#editar');
    btnEditar?.dispatchEvent( new Event('click') );

    expect(component.eventoEditar.emit).toHaveBeenCalled();

  });

  it('debe emitir eventoBorrar con el botón eliminar', () => {

    component.superheroe = {"id": "a2", "nombre": "Manolo", "poder": "superfuerza", "imagen": "www.example.com/image.jpg"};

    fixture.detectChanges();
    spyOn( component.eventoBorrar, 'emit');

    const btnEliminar = compiled.querySelector('#eliminar');
    btnEliminar?.dispatchEvent( new Event('click') );

    expect(component.eventoBorrar.emit).toHaveBeenCalled();

  });
  
});
