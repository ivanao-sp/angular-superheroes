import { CommonModule, TitleCasePipe } from '@angular/common';
import { Component, TemplateRef } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCheck, faXmark, faSquarePlus } from '@fortawesome/free-solid-svg-icons';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ModalMensajeComponent } from '../shared/modal-mensaje/modal-mensaje.component';
import { superheroe } from '../../interfaces/superheroe';
import { DataService } from '../../services/data/data.service';
import { CardComponent } from '../card/card.component';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from 'ngx-mgmg-filter-pipe';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-listado',
  standalone: true,
  imports: [ FontAwesomeModule, CommonModule, CardComponent, FormsModule, FilterPipe, PaginationModule, TitleCasePipe, RouterLink ],
  templateUrl: './listado.component.html',
  styleUrl: './listado.component.scss',
  providers: [ BsModalService ]
})
export class ListadoComponent {
  faCheck = faCheck;
  faXmark = faXmark;
  faSquarePlus = faSquarePlus;
  modalRef?: BsModalRef;
  superherores: superheroe[] = [];
  inputBuscador = '';
  euIndexOrder: number = 0;
  elementosPorPagina: number = 6;
  paginaActual: number = 1;
  superheroeModal: superheroe = { id: "", imagen: "", nombre: "", poder: "" };
  banderaLoading: boolean = true;

  constructor(private dataService: DataService, private modalService: BsModalService, private router: Router) {}

  ngOnInit(): void {
    this.getSuperheroes();
  }

  getSuperheroes() {
    this.banderaLoading = true;
    this.dataService.get("superheroes").subscribe((data: any[]) => {
      this.superherores = data.map(({ _id, name, age, colour}) => ({
        "id": _id,
        "imagen": colour,
        "nombre": name,
        "poder": age
      }));
      this.banderaLoading = false;
    }, error => {
      this.ErrorPeticion();
    });
  }

  editarSuperheroe(heroe: superheroe) {
    this.router.navigate(['/editar/'+heroe.id]);
  }

  borrarSuperheroe() {
    this.banderaLoading = true;
    this.dataService.delete("superheroes", this.superheroeModal.id).subscribe((data: any[]) => {
      this.getSuperheroes();
      this.paginaActual = 1;
      this.banderaLoading = false;
    }, error => {
      this.ErrorPeticion();
    });
  }

  crearDatos() {
    this.dataService.crearDatos().subscribe((data: any[]) => {
      console.log(data);
      this.getSuperheroes();
    }, error => {
      this.ErrorPeticion();
    });
  }

  ErrorPeticion() {
    const initialState = {
      error: true,
      mensaje: "A Ocurrido un error al realizar la petición. Asegurese de que dispone de peticiones para usar la API y que su path no ha caducado."
    };
    this.modalRef = this.modalService.show(ModalMensajeComponent, {initialState});
    this.modalRef.content.closeBtnName = 'Close';
  }

  modalBorrar(superheroe: superheroe, template: TemplateRef<any>) {
    this.superheroeModal = superheroe;
    this.modalRef = this.modalService.show(template);
  }
  
}
