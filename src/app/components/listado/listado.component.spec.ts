import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { Component, TemplateRef } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCheck, faXmark, faSquarePlus } from '@fortawesome/free-solid-svg-icons';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ModalMensajeComponent } from '../shared/modal-mensaje/modal-mensaje.component';
import { superheroe } from '../../interfaces/superheroe';
import { DataService } from '../../services/data/data.service';
import { CardComponent } from '../card/card.component';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from 'ngx-mgmg-filter-pipe';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Router, ActivatedRoute, RouterLink, RouterModule } from '@angular/router';
import { ListadoComponent } from './listado.component';

describe('ListadoComponent', () => {
  let component: ListadoComponent;
  let fixture: ComponentFixture<ListadoComponent>;

  const fakeActivatedRoute = {
    snapshot: { data: { } }
  } as ActivatedRoute;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ ListadoComponent, HttpClientModule ],
      providers: [ {provide: ActivatedRoute, useValue: fakeActivatedRoute} ]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
