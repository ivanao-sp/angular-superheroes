import { superheroeAPI } from '../../interfaces/superheroeAPI';

export var datosParaAPI: superheroeAPI[] = [

    {
        "name": "Pepita",
        "age": "visión calorifica",
        "colour": "https://cdn.pixabay.com/photo/2024/04/16/23/45/ai-generated-8701021_1280.jpg"
    },
    {
        "name": "Manolete",
        "age": "super velocidad",
        "colour": "https://cdn.pixabay.com/photo/2024/04/16/23/45/ai-generated-8701019_1280.jpg"
    },
    {
        "name": "Pepito",
        "age": "super fuerza",
        "colour": "https://cdn.pixabay.com/photo/2023/04/17/13/30/ai-generated-7932463_1280.jpg"
    },
    {
        "name": "Super Noelia",
        "age": "Curacion",
        "colour": "https://cdn.pixabay.com/photo/2024/05/09/13/06/ai-generated-8750995_960_720.jpg"
    },
    {
        "name": "Elsa Pataqui",
        "age": "Volar",
        "colour": "https://cdn.pixabay.com/photo/2014/11/16/23/39/superhero-534120_1280.jpg"
    },
    {
        "name": "super Nacho",
        "age": "Super resistencia",
        "colour": "https://cdn.pixabay.com/photo/2024/03/27/21/57/ai-generated-8660011_1280.jpg"
    },
    {
        "name": "super Einstein",
        "age": "Super inteligencia",
        "colour": "https://cdn.pixabay.com/photo/2024/05/06/01/25/ai-generated-8742239_1280.jpg"
    },
    {
        "name": "super Carlos",
        "age": "Telequinesis",
        "colour": "https://cdn.pixabay.com/photo/2024/02/16/20/16/ai-generated-8578298_1280.jpg"
    },
    {
        "name": "Andrea Lopez",
        "age": "ver el futuro",
        "colour": "https://cdn.pixabay.com/photo/2024/02/12/04/36/ai-generated-8567771_1280.jpg"
    },
    {
        "name": "Super Lola",
        "age": "Super inteligencia",
        "colour": "https://cdn.pixabay.com/photo/2023/12/22/23/46/ai-generated-8464622_1280.jpg"
    }
];