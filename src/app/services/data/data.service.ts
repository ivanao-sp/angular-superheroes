import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { datosParaAPI } from './data';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private static pathEndpoins: string = "0f7a21c467554129aacd3570bfb2ad3c";

  private static URLs: { [key: string]: string } = { "superheroes": "https://crudcrud.com/api/"+DataService.pathEndpoins+"/users", "imagenAleatoria": "https://loremflickr.com/500/500/marvel,cosplay,anime"};

  constructor(private http: HttpClient) { }

  get(tipo: string, id: string = ""): Observable<any> {
    return this.http.get(DataService.URLs[tipo]+id);
  }

  post(tipo: string, datos: any): Observable<any> {
    return this.http.post(DataService.URLs[tipo], datos);
  }

  put(tipo: string, id: string, datos: any): Observable<any> {
    return this.http.put(DataService.URLs[tipo]+id, datos);
  }

  delete(tipo: string, clave: any): Observable<any> {
    return this.http.delete(DataService.URLs[tipo]+"/"+clave);
  }

  crearDatos(): Observable<any> {
    let llamdasAPI: any[] = [];
    datosParaAPI.forEach(elemento => {
      llamdasAPI.push(this.http.post(DataService.URLs["superheroes"], elemento));
    });
    return forkJoin(llamdasAPI);
  }

  getPathEndpoins():string {
    return DataService.pathEndpoins;
  }

}
