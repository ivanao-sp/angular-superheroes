import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { superheroeAPI } from '../../interfaces/superheroeAPI';
import { DataService } from './data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('DataService', () => {
  let service: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('debería comprobar la estructura del JSON en función GET', () => {
    const httpMock = TestBed.inject(HttpTestingController);
    const servicio = TestBed.inject(DataService);
  
    const json = '{"_id": "a2", "name": "Manolo", "age": "superfuerza", "colour": "www.example.com/image.jpg"}';
  
    servicio.get("superheroes").subscribe((response) => {
      expect(response).toEqual({_id: "a2", name: "Manolo", age: "superfuerza", colour: "www.example.com/image.jpg"});
    });
  
    const req = httpMock.expectOne("https://crudcrud.com/api/"+servicio.getPathEndpoins()+"/users");
    expect(req.request.method).toBe('GET');
    req.flush(JSON.parse(json));
  
    httpMock.verify();
  });

  it('debería comprobar la estructura del JSON en función POST', () => {
    const httpMock = TestBed.inject(HttpTestingController);
    const servicio = TestBed.inject(DataService);
  
    const json = '{"_id": "a2", "name": "Manolo", "age": "superfuerza", "colour": "www.example.com/image.jpg"}';
    const datos = {"name": "Manolo", "age": "superfuerza", "colour": "www.example.com/image.jpg"}
  
    servicio.post("superheroes", datos).subscribe((response) => {
      expect(response).toEqual({_id: "a2", name: "Manolo", age: "superfuerza", colour: "www.example.com/image.jpg"});
    });
  
    const req = httpMock.expectOne("https://crudcrud.com/api/"+servicio.getPathEndpoins()+"/users");
    expect(req.request.method).toBe('POST');
    req.flush(JSON.parse(json));
  
    httpMock.verify();
  });

});
