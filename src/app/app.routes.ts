import { Routes } from '@angular/router';
import { ListadoComponent } from "./components/listado/listado.component";
import { CrearEditarComponent } from "./components/crear-editar/crear-editar.component"; 

export const routes: Routes = [
    { path: 'listado', component: ListadoComponent },
    { path: 'crear', component: CrearEditarComponent },
    { path: 'editar/:id', component: CrearEditarComponent },
    { path: '**',  pathMatch: 'full', redirectTo: 'listado'},
];
